Source: ppx-deriving
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Stéphane Glondu <glondu@debian.org>,
 Ralf Treinen <treinen@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml-nox,
 ocaml-dune,
 cppo,
 ppxfind,
 libfindlib-ocaml-dev,
 libresult-ocaml-dev,
 libppx-tools-ocaml-dev (>= 4.02.3),
 libounit-ocaml-dev,
 libppx-derivers-ocaml-dev,
 libmigrate-parsetree-ocaml-dev,
 dh-ocaml
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/ocaml-team/ppx-deriving
Vcs-Git: https://salsa.debian.org/ocaml-team/ppx-deriving.git
Homepage: https://github.com/whitequark/ppx_deriving

Package: libppx-deriving-ocaml-dev
Provides:
 ${ocaml:Provides}
Architecture: any
Depends:
 ocaml-findlib,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Description: type-driven code generation for OCaml (dev files)
 Ppx_deriving provides common infrastructure for generating code based
 on type definitions, and a set of useful plugins for common tasks.
 .
 This package contains development files.

Package: libppx-deriving-ocaml
Provides:
 ${ocaml:Provides}
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Description: type-driven code generation for OCaml (runtime files)
 Ppx_deriving provides common infrastructure for generating code based
 on type definitions, and a set of useful plugins for common tasks.
 .
 This package contains runtime files.
